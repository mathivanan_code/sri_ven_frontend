import React,{useState,useEffect} from "react";
import axios from 'axios';


// Importing toastify module
import {toast} from 'react-toastify';

// Import toastify css file
import 'react-toastify/dist/ReactToastify.css';

// toast-configuration method,
// it is compulsory method.
toast.configure();


const DueDate = () => {
    const intialstate =  {
        leggerNumber:'',
        loanNumber:'',
        loanDate:'',
        loanDueDate:'',
        noInstallment:'',
        dueAmount:'',
        hireName:'',
        hireMobileNo:'',
        hireAlterNumber:'',
    };
    const [dueDate,setDueDate] = useState(intialstate);

    const submitHandler = async(e) =>{
        e.preventDefault();
console.log(dueDate);
        // const instance = axios.create(
        //     {
        //         baseURL: "http://localhost:8000//api",
        //         withCredentials: false,
        //         headers: {
        //             'Access-Control-Allow-Origin' : '*',
        //             'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        //         }
        //     })

        await  axios.post('/legger-submit',dueDate)
            .then(res => {
                console.log(res.data);
                if(res.data.status == true){
                    toast(res.data.statusString);
                    setDueDate(intialstate);
                }else{
                    toast(res.data.statusString);
                }
            })
            .catch(error => {
                console.log(error);
            })
    };
    return (
      <form className = "ui form" onSubmit={submitHandler} >
        <h2>Due Date Tracker </h2>
        <div className="two fields">
          <div className="field">
            <label for="leggerNumber">Enter Legger Number</label>
            <input id="leggerNumber" type = "number" placeholder="Enter Legger Number" name="leggerNumber" value={dueDate.leggerNumber}  onChange={e => setDueDate({...dueDate,leggerNumber:e.target.value})}  />
          </div>
          <div className="field">
            <label for="loanNumber">Enter Loan Number</label>
            <input id="loanNumber" type = "number" placeholder="Enter Loan Number" name="loanNumber"  value={dueDate.loanNumber}  onChange={e => setDueDate({...dueDate,loanNumber:e.target.value})}   />
          </div>
        </div>
        <div className="two fields">
          <div className="field">
            <label for="loanDate">Loan Distribution Date</label>
            <input  id="loanDate" type = "date" placeholder="Loan Date" name="loanDate"   value={dueDate.loanDate}  onChange={e => setDueDate({...dueDate,loanDate:e.target.value})}   />
          </div>
          <div className="field">
              <label for="loanDueDate">Due Date</label>
              <input  id="loanDueDate" type = "date" placeholder="Loan Due Date" name="loanDueDate"  value={dueDate.loanDueDate}   onChange={e => setDueDate({...dueDate,loanDueDate:e.target.value})}  />
          </div>
        </div>
        <div className="two fields">
          <div className="field">
            <label for="noInstallment">Number of Installment</label>
            <input  id="noInstallment" type = "number" placeholder="Installment" name="noInstallment"   value={dueDate.noInstallment} onChange={e => setDueDate({...dueDate,noInstallment:e.target.value})}  />
          </div>
          <div className="field">
              <label for="dueAmount">Due Amount</label>
              <input  id="dueAmount" type = "number" placeholder="Due Amount" name="dueAmount"  value={dueDate.dueAmount}  onChange={e => setDueDate({...dueDate,dueAmount:e.target.value})}  />
          </div>
        </div>
        <div className="field">
          <label for="hireName">Name</label>
          <input id="hireName" type = "text" placeholder="Hire's Name" name="hireName"   value={dueDate.hireName}  onChange={e => setDueDate({...dueDate,hireName:e.target.value})}   />
        </div>
        <div className="two fields">
          <div className="field">
            <label for="hireMobileNo">Hire's Mobile Number</label>
            <input  id="hireMobileNo" type = "number" placeholder="Hire's Contact Number" name="hireMobileNo"  value={dueDate.hireMobileNo} maxLength="11"   onChange={e => setDueDate({...dueDate,hireMobileNo:e.target.value})}   />
          </div>
          <div className="field">
              <label for="hireAlterNumber">Hire's Alternate Mobile Number</label>
              <input  id="hireAlterNumber" type = "number" placeholder="Hire's Alternate Number" name="hireAlterNumber"  value={dueDate.hireAlterNumber} maxLength="11"  onChange={e => setDueDate({...dueDate,hireAlterNumber:e.target.value})}   />
          </div>
        </div>
        <button className="ui button" type="submit"  >Submit</button>
          {/*<h2>{JSON.stringify(dueDate)}</h2>*/}
        </form>
    )
}

export default DueDate;
