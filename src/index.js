import React from "react";
import ReactDOM from "react-dom";
import faker from 'faker';
import DueDate from './DueDate';
import DueDateListing from './DueDateListing';
import 'semantic-ui-css/semantic.min.css'
import { Form } from "semantic-ui-react";
import './index.css'
import {BrowserRouter as Router ,Switch,Route} from 'react-router-dom';
import Sidebar from './Sidebar';
class App extends React.Component {

    render() {
        return (
            <
            div className = "main ui container" >
                {/*<Sidebar/>*/}
                <Router>
                    <Switch>
                        <Route path="/duedatalisting/">
                            <DueDateListing/>
                        </Route>
                        <Route path="/">
                            <DueDate/>
                        </Route>
                    </Switch>
                </Router>
            </div >

        )
    }
}


ReactDOM.render( <App/> ,
    document.querySelector('#root')
);
