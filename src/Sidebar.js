import React from   'react';
import {useHistory} from 'react-router-dom';

const Sidebar = () =>{
    const history = useHistory();
    const selectChannel = (id) => {
        console.log(id);
        history.push("/duedatalisting");
    };

    return (
        <div className="menu_bar">
            <ul>
                <li onClick={()=>selectChannel('/duedatalisting')}>Due date</li>
                <li onClick={()=>selectChannel('/')}>Due Date List</li>
            </ul>
        </div>
    )
}

export default Sidebar;
