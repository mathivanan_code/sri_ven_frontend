import React,{useEffect,useState} from "react";
import axios from 'axios';
import {toast} from "react-toastify";

const DueDateListing = () => {
const [leggerLists,setLeggerLists]=useState([]);
    useEffect(()=>{
          axios.get('/legger-list')
            .then(res => {
                console.log(res.data.lists);
             setLeggerLists(res.data.lists);
            })
            .catch(error => {
                console.log(error);
            })
    },[]);
    return (
      <div>
        <form className="ui form">
        <div className="field">
          <label for="dueDateListingSelect">Due Date</label>
          <input id="dueDateListingSelect" type = "date" placeholder="Due Date" name="dueDateListingSelect" />
        </div>
          <button className="ui button" type="submit">Submit</button>
        <table className="ui celled table">
        <thead>

          <tr>
            <th rowspan="2">Name</th>
            <th rowspan="2">Loan Number</th>
            <th rowspan="2">Phone 1</th>
            <th rowspan="2">Phone 2</th>
            <th rowspan="2">Due Amount</th>
            <th colspan="2">Response / Paid</th>
            <th rowspan="2">Remarks</th>
            <th rowspan="2">Action</th>
          </tr>
          <tr>
          <th>Yes</th>
          <th>NO & Date</th>
          </tr>
        </thead>
        <tbody>
        {leggerLists.map(l_list => {
           return (
            <tr>
                <td>{l_list.hirename}</td>
                <td>{l_list.loan_no}</td>
                <td>{l_list.hire_mob_no}</td>
                <td>{l_list.hire_mob_no}</td>
                <td>{l_list.due_amount}</td>
                <td><input type="checkbox" tabIndex="0" className="hidden"/></td>
                <td><input type="date" tabIndex="0" className="hidden"/></td>
                <td><textarea type="remarks" tabIndex="0" className="hidden"></textarea></td>
                <td>
                    <button className="ui button" type="submit">Submit</button>
                </td>
            </tr>
        )
        })}


        </tbody>
        </table>
        </form>
      </div>

    )
  }

  export default DueDateListing;
